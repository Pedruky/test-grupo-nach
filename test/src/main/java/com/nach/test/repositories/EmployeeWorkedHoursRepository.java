package com.nach.test.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nach.test.entities.EmployeeWorkedHours;
import com.nach.test.entities.Employees;

public interface EmployeeWorkedHoursRepository extends JpaRepository<EmployeeWorkedHours, Long>{
	EmployeeWorkedHours findFirstByOrderByIdDesc();
	List<EmployeeWorkedHours> findByEmployee(Employees employee);
	List<EmployeeWorkedHours> findByDate(Date date);
}
