package com.nach.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nach.test.entities.Employees;
import com.nach.test.entities.Jobs;

public interface JobRepository extends JpaRepository<Jobs, Long>{
	Jobs findByid(Long id);
	Jobs findByEmployee(Employees employee);
}
