package com.nach.test.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nach.test.entities.Genders;

public interface GenderRepository extends JpaRepository<Genders, Long>{
	Genders findByid(Long id);
}
