package com.nach.test.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nach.test.entities.Employees;

@Repository
public interface EmployeeRepository extends JpaRepository<Employees, Long>{
	
	List<Employees> findByLastNameAndName(String lastName, String name);
	Employees findByid(Long id);

}
