package com.nach.test.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import com.nach.test.entities.Employees;
import com.nach.test.entities.Genders;
import com.nach.test.entities.Jobs;
import com.nach.test.repositories.EmployeeRepository;
import com.nach.test.repositories.GenderRepository;
import com.nach.test.repositories.JobRepository;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private GenderRepository genderRepository;
	
	@Autowired
	private JobRepository jobRepository;
	
	@PostMapping()
	public JSONObject createEmployee(@RequestBody JSONObject employee) {
		//Comprobar edad
		Date currentDate = new Date();
		String[] birthdateCurrent = employee.get("birthdate").toString().split("-");
		int years = currentDate.getYear() - Integer.parseInt(birthdateCurrent[0]);
		int months = currentDate.getMonth() - Integer.parseInt(birthdateCurrent[1]);
		int days = currentDate.getDay() - Integer.parseInt(birthdateCurrent[2]);
		if(years < 18) {
			JSONObject json = new JSONObject();
			json.put("id", null);
			json.put("success", false);
			return json;
		}
		if(years == 18 && months < 0) {
			JSONObject json = new JSONObject();
			json.put("id", null);
			json.put("success", false);
			return json;
		}
		if(years == 18 && months == 0 && days < 0) {
			JSONObject json = new JSONObject();
			json.put("id", null);
			json.put("success", false);
			return json;
		}
		//Comprobar si el nombre y los apellidos existen
		List<Employees> employeesFinded = this.employeeRepository.findByLastNameAndName(employee.get("last_name").toString(), employee.get("name").toString());
		if(employeesFinded.size() > 0) {
			JSONObject json = new JSONObject();
			json.put("id", employeesFinded.get(0).getId());
			json.put("success", false);
			return json;
		}else{
			//Comprobar si el puesto existe
			if(!this.jobRepository.existsById(Long.parseLong(employee.get("job_id").toString()))){
				JSONObject json = new JSONObject();
				json.put("id", null);
				json.put("success", false);
				return json;
			}else {
				//Comprobar si el genero existe
				if(!this.genderRepository.existsById(Long.parseLong(employee.get("gender_id").toString()))){
					JSONObject json = new JSONObject();
					json.put("id", null);
					json.put("success", false);
					return json;
				}else{
					currentDate.setYear(years);
					currentDate.setMonth(months);
					currentDate.setDate(days);
				}
			}
		}
		Employees employeeEntity = new Employees();
		employeeEntity.setName(employee.get("name").toString());
		employeeEntity.setLastName(employee.get("lastName").toString());
		employeeEntity.setGender(this.genderRepository.findByid(Long.parseLong(employee.get("gender_id").toString())));
		employeeEntity.setJob(this.jobRepository.findByid(Long.parseLong(employee.get("job_id").toString())));
		employeeEntity.setBirthdate(currentDate);
		this.employeeRepository.save(employeeEntity);
		JSONObject json = new JSONObject();
		json.put("id", null);
		json.put("success", true);
		return json;
	}

}
