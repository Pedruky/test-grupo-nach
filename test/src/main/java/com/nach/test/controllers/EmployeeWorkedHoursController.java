package com.nach.test.controllers;

import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nach.test.entities.EmployeeWorkedHours;
import com.nach.test.entities.Employees;
import com.nach.test.entities.Jobs;
import com.nach.test.repositories.EmployeeRepository;
import com.nach.test.repositories.EmployeeWorkedHoursRepository;
import com.nach.test.repositories.JobRepository;

@RestController
@RequestMapping("/employeeHours")
public class EmployeeWorkedHoursController {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private EmployeeWorkedHoursRepository employeeWorkedHoursRepository;
	
	@Autowired
	private JobRepository jobRepository;
	
	@PostMapping()
	public JSONObject createEmployeeWorkedHours(@RequestBody JSONObject employeeWorkedHours) {
		//Comprobar que el empleado exista
		Employees employee = this.employeeRepository.findByid(Long.parseLong(employeeWorkedHours.get("employee_id").toString()));
		if(employee == null){
			JSONObject json = new JSONObject();
			json.put("id", null);
			json.put("success", false);
			return json;
		}
		//Comprobar que las horas trabajadas sean menor que 20
		if(Long.parseLong(employeeWorkedHours.get("worked_hours").toString()) > 20){
			JSONObject json = new JSONObject();
			json.put("id", null);
			json.put("success", false);
			return json;
		}
		//Comprobar que el día de trabajo sea igual o menor al actual
		String[] dateAdded = employeeWorkedHours.get("worked_date").toString().split("-");
		Date dateToAdd = new Date( Integer.parseInt(dateAdded[0]), Integer.parseInt(dateAdded[1]), Integer.parseInt(dateAdded[2]) );
		EmployeeWorkedHours lastEmployeeWorkedHours = this.employeeWorkedHoursRepository.findFirstByOrderByIdDesc();
		if(dateToAdd.after(lastEmployeeWorkedHours.getDate())) {
			JSONObject json = new JSONObject();
			json.put("id", null);
			json.put("success", false);
			return json;
		}
		//Comprobar que el empleado no tenga registrado horas el mismo día
		List<EmployeeWorkedHours> hoursFinded = this.employeeWorkedHoursRepository.findByDate(dateToAdd);
		if(hoursFinded.size() > 0){
			JSONObject json = new JSONObject();
			json.put("id", null);
			json.put("success", false);
			return json;
		}
		EmployeeWorkedHours employeeWorkedHoursToAdd = new EmployeeWorkedHours();
		employeeWorkedHoursToAdd.setEmployee(employee);
		employeeWorkedHoursToAdd.setWorkedHours(Long.parseLong(employeeWorkedHours.get("worked_hours").toString()));
		employeeWorkedHoursToAdd.setDate(dateToAdd);
		this.employeeWorkedHoursRepository.save(employeeWorkedHoursToAdd);
		EmployeeWorkedHours addedEmployeeWorkedHours = this.employeeWorkedHoursRepository.findFirstByOrderByIdDesc();
		JSONObject json = new JSONObject();
		json.put("id", addedEmployeeWorkedHours.getId());
		json.put("success", true);
		return json;
	}
	
	@PostMapping("/getHours")
	public JSONObject getHours(@RequestBody JSONObject employeeDates) {
		//Comprobar la fecha de inicio con la final
		String[] startDate = employeeDates.get("start_date").toString().split("-");
		String[] endDate = employeeDates.get("end_date").toString().split("-");
		int years = Integer.parseInt(endDate[0]) - Integer.parseInt(startDate[0]);
		int months = Integer.parseInt(endDate[1]) - Integer.parseInt(startDate[1]);
		int days = Integer.parseInt(endDate[2]) - Integer.parseInt(startDate[2]);
		if(years < 0) {
			JSONObject json = new JSONObject();
			json.put("total_worked_hours", null);
			json.put("success", false);
			return json;
		}
		if(years == 0 && months < 0) {
			JSONObject json = new JSONObject();
			json.put("total_worked_hours", null);
			json.put("success", false);
			return json;
		}
		if(years == 0 && months == 0 && days < 0) {
			JSONObject json = new JSONObject();
			json.put("total_worked_hours", null);
			json.put("success", false);
			return json;
		}
		//Comprobar si existe el empleado
		Employees employee = this.employeeRepository.findByid(Long.parseLong(employeeDates.get("employee_id").toString()));
		if(employee == null) {
			JSONObject json = new JSONObject();
			json.put("total_worked_hours", null);
			json.put("success", false);
			return json;
		}
		List<EmployeeWorkedHours> hours = this.employeeWorkedHoursRepository.findByEmployee(employee);
		Date Sdate = new Date(Integer.parseInt(startDate[0]), Integer.parseInt(startDate[1]), Integer.parseInt(startDate[2]));
		Date Edate = new Date(Integer.parseInt(endDate[0]), Integer.parseInt(endDate[1]), Integer.parseInt(endDate[2]));
		long totalHours = 0;
		for(int index=0; index < hours.size(); index++) {
			if(hours.get(index).getDate().after(Sdate) || hours.get(index).getDate().equals(Sdate)){
				if(hours.get(index).getDate().before(Edate) || hours.get(index).getDate().equals(Edate)) {
					totalHours = totalHours + hours.get(index).getWorkedHours();
				}
			}
			
		}
		JSONObject json = new JSONObject();
		json.put("total_worked_hours", totalHours);
		json.put("success", true);
		return json;
	}
	
	@PostMapping("/getTotalSalary")
	public JSONObject getTotalSalary(@RequestBody JSONObject employeeDates) {
		//Comprobar la fecha de inicio con la final
		String[] startDate = employeeDates.get("start_date").toString().split("-");
		String[] endDate = employeeDates.get("end_date").toString().split("-");
		int years = Integer.parseInt(endDate[0]) - Integer.parseInt(startDate[0]);
		int months = Integer.parseInt(endDate[1]) - Integer.parseInt(startDate[1]);
		int days = Integer.parseInt(endDate[2]) - Integer.parseInt(startDate[2]);
		if(years < 0) {
			JSONObject json = new JSONObject();
			json.put("total_worked_hours", null);
			json.put("success", false);
			return json;
		}
		if(years == 0 && months < 0) {
			JSONObject json = new JSONObject();
			json.put("total_worked_hours", null);
			json.put("success", false);
			return json;
		}
		if(years == 0 && months == 0 && days < 0) {
			JSONObject json = new JSONObject();
			json.put("total_worked_hours", null);
			json.put("success", false);
			return json;
		}
		//Comprobar si existe el empleado
		Employees employee = this.employeeRepository.findByid(Long.parseLong(employeeDates.get("employee_id").toString()));
		if(employee == null) {
			JSONObject json = new JSONObject();
			json.put("total_worked_hours", null);
			json.put("success", false);
			return json;
		}
		List<EmployeeWorkedHours> hours = this.employeeWorkedHoursRepository.findByEmployee(employee);
		Date Sdate = new Date(Integer.parseInt(startDate[0]), Integer.parseInt(startDate[1]), Integer.parseInt(startDate[2]));
		Date Edate = new Date(Integer.parseInt(endDate[0]), Integer.parseInt(endDate[1]), Integer.parseInt(endDate[2]));
		long totalHours = 0;
		for(int index=0; index < hours.size(); index++) {
			if(hours.get(index).getDate().after(Sdate) || hours.get(index).getDate().equals(Sdate)){
				if(hours.get(index).getDate().before(Edate) || hours.get(index).getDate().equals(Edate)) {
					totalHours = totalHours + hours.get(index).getWorkedHours();
				}
			}
			
		}
		Jobs job = this.jobRepository.findByEmployee(employee);
		double totalSalary = totalHours * job.getSalary();
		JSONObject json = new JSONObject();
		json.put("total_worked_hours", totalHours);
		json.put("success", true);
		return json;
	}

}
