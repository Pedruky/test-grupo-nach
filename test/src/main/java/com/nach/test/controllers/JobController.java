package com.nach.test.controllers;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nach.test.entities.Jobs;
import com.nach.test.repositories.JobRepository;

@RestController
@RequestMapping("/jobs")
public class JobController {
	
	@Autowired
	private JobRepository jobRepository;
	
	@PostMapping()
	public JSONObject getEmployeesInJob(@RequestBody JSONObject job) {
		//Comprobar si el trabajo existe
		if( !this.jobRepository.existsById(Long.parseLong(job.get("job_id").toString())) ){
			JSONObject json = new JSONObject();
			json.put("success", false);
			return json;
		}
		Jobs jobSelect = this.jobRepository.findByid(Long.parseLong(job.get("job_id").toString()));
		JSONObject json = new JSONObject();
		json.put("employees", jobSelect);
		json.put("success", false);
		return json;
	}

}
