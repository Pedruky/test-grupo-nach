package com.nach.test.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "employee_worked_hours")
public class EmployeeWorkedHours {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employees employee_id;
	
	@Column(name = "worked_hours")
    private Long workedHours;
	
	@Column(name = "worked_date")
	@Temporal(TemporalType.DATE)
	private Date date;
	
	public EmployeeWorkedHours() {
		
	}

	public EmployeeWorkedHours(Long id, Employees employee_id, Long workedHours, Date date) {
		super();
		this.id = id;
		this.employee_id = employee_id;
		this.workedHours = workedHours;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employees getEmployee() {
		return employee_id;
	}

	public void setEmployee(Employees employee_id) {
		this.employee_id = employee_id;
	}

	public Long getWorkedHours() {
		return workedHours;
	}

	public void setWorkedHours(Long workedHours) {
		this.workedHours = workedHours;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
