package com.nach.test.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "employees")
public class Employees {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="gender_id")
	private Genders gender_id;
	
	@ManyToOne
	@JoinColumn(name="job_id")
	private Jobs job_id;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "birthdate")
	@Temporal(TemporalType.DATE)
	private Date birthdate;
	
	@OneToMany(mappedBy = "employee_id")
    private Collection<EmployeeWorkedHours> employee_worked_hour;
	
	public Employees() {
		
	}

	public Employees(Long id, Genders gender_id, Jobs job_id, String name, String lastName, Date birthdate) {
		super();
		this.id = id;
		this.gender_id = gender_id;
		this.job_id = job_id;
		this.name = name;
		this.lastName = lastName;
		this.birthdate = birthdate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Genders getGender() {
		return gender_id;
	}

	public void setGender(Genders gender_id) {
		this.gender_id = gender_id;
	}

	public Jobs getJob() {
		return job_id;
	}

	public void setJob(Jobs job_id) {
		this.job_id = job_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
}
